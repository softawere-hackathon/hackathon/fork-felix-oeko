from time import sleep
import stressinjector as injector
from cpu import CPUStress

injector.CPUStress = CPUStress

if __name__ == '__main__':
    for i in range(60):
        CPUStress(seconds=20, cores=i+1)
        sleep(20)
